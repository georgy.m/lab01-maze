import numpy as np
import unittest
import maze

_maze_degenerate = '''0 0\n1'''
_maze_noexit = '''1 1
0 0 0
0 1 0
0 0 0'''
_maze_noexit_2 = '''1 1
1 0 1
0 1 0
1 0 1'''
_maze_regular_small = '''2 1
0 0 1 0
0 1 1 0
0 1 1 0
0 1 0 0
1 0 0 1'''
_maze_regular = '''1 1
0 0 0 0 0 0
0 1 1 0 1 0
0 0 1 0 1 1
0 0 1 1 0 0
0 0 1 1 1 0
0 0 0 1 1 0
0 0 0 1 1 1
1 1 1 1 1 1'''


class TestMazeParsing(unittest.TestCase):
    def read(self, maze_desc: str, correct_array: np.ndarray,
             correct_start: tuple):
        maze_array, start = maze.read_maze(maze_desc)
        assert np.all(maze_array == correct_array)
        assert correct_start == start

    def test_degenerate(self):
        array = np.array([[1]])
        start_corr = (0, 0)
        self.read(_maze_degenerate, array, start_corr)

    def test_noexit(self):
        array = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]])
        start_corr = (1, 1)
        self.read(_maze_noexit, array, start_corr)

    def test_noexit_2(self):
        array = np.array([[1, 0, 1], [0, 1, 0], [1, 0, 1]])
        start_corr = (1, 1)
        self.read(_maze_noexit_2, array, start_corr)

    def test_regular_small(self):
        array = np.array([[0, 0, 1, 0], [0, 1, 1, 0], [0, 1, 1, 0],
                          [0, 1, 0, 0], [1, 0, 0, 1]])
        start_corr = (2, 1)
        self.read(_maze_regular_small, array, start_corr)


class TestMazeSolver(unittest.TestCase):
    def solve(self, maze_desc: str, correct_result: int):
        res = maze.solve_maze(maze_desc, diag=False)
        assert res == correct_result

    def test_degenerate(self):
        self.solve(_maze_degenerate, 0)

    def test_noexit(self):
        self.solve(_maze_noexit, -1)

    def test_noexit_2(self):
        self.solve(_maze_noexit_2, -1)

    def test_maze_regular_small(self):
        self.solve(_maze_regular_small, 3)

    def test_maze_regular(self):
        self.solve(_maze_regular, 8)


if __name__ == '__main__':
    unittest.main()
