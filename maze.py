'''Meshcheryakov Georgy
I believe that the task formulation was not crystal clear, as some points
remained unlit regarding the requirements to pass the task. At least that's
so for me. To be more precise, I found no explanations on the following:
    1. Can we move in a diagonal direction (x + 1, y + 1, for instance)?
    2. What exactly is considered to be a way out of the maze?
As no clarification was present, I let (1) to be parametetrised by  "diag"
parameters, and I assumed that (2) exit is any cell on the "edge" of the
maze (x = 0, arbitrary y; x = max_x, arbitrary y; and vice-versa for y).
'''
from collections import deque
import re
import os
import numpy as np


def read_maze(buffer: str):
    '''Translates a maze in a textual form into np.array for further use.
    Keyword arguments:
        buffer -- A stirng containing maze structure or a name of a file
                  that contains the aforementioned stirng.
    Returns:
        A 2D matrix (np.array) description of the maze.
    '''
    ptrn = '\\' + 'd'
    if os.path.isfile(buffer):
        with open(buffer, 'r') as f:
            buffer = f.read()
    lines = (line for line in buffer.splitlines() if len(line.strip()))
    start_x, start_y = map(int, re.findall(ptrn, next(lines)))
    maze = [list(map(int, re.findall(ptrn, line))) for line in lines]
    maze = np.array(maze)
    return maze, (start_x, start_y)


def solve_maze(maze, start=None, diag=False):
    '''Returns a length of the shortest path to exit (if any, -1 otherwise).
    Keyword arguments:
        maze  -- Either an np.array, or string containing maze structure and
                 starting position (numberation starts with 0) at the first
                 line, or a filename containing the aforementioned string.
        start -- Starting position (numerations starts with 0). Ignored if
                 maze is not np.array.
        diag  -- Allows the virtual traveller to move in a diagonal directions.
    Returns:
        A length of shortest path to exit if any, -1 otherwise.
    '''
    if not isinstance(maze, np.ndarray):
        maze, start = read_maze(maze)
    q = deque([start])
    shortest_path = np.inf
    while q:
        x, y = q.popleft()
        dist = maze[x, y] >> 1
        if is_exit(maze, x, y):
            if dist < shortest_path:
                shortest_path = dist
        for x, y in iter_adjacents(maze, x, y, diag=diag):
            q.append((x, y))
            maze[x, y] = (dist + 1) << 1
    if np.isinf(shortest_path):
        return -1
    return shortest_path


def iter_adjacents(maze: np.ndarray, x: int, y: int, diag=False):
    '''Iterates over adjacent cells (as long as they are not marked as
    visited).
    Keyword arguments:
        maze -- A maze as returned from read_maze.
        x    -- An "x" coordinate of the cell.
        y    -- An "y" coordinate of the cell.
        diag -- If true, then diagonal adjacent cells are also returned.
    Returns:
        Unvisited cells adjacent to (x, y).
    '''
    max_x, max_y = maze.shape
    rx, lx = x + 1, x - 1
    uy, dy = y + 1, y - 1
    right = rx < max_x
    left = lx >= 0
    up = uy < max_y
    down = dy >= 0
    if right:
        if maze[rx, y] == 1:
            yield rx, y
        if diag and down and maze[rx, dy] == 1:
            yield rx, dy
    if down:
        if maze[x, dy] == 1:
            yield x, dy
        if diag and left and maze[lx, dy] == 1:
            yield lx, dy
    if left:
        if maze[lx, y] == 1:
            yield lx, y
        if diag and up and maze[lx, uy] == 1:
            yield lx, uy
    if up:
        if maze[x, uy] == 1:
            yield x, uy
        if diag and right and maze[rx, uy] == 1:
            yield rx, uy


def is_exit(maze: np.array, x: int, y: int):
    '''Checks if cell is on the "edge" of maze, i.e. if it leads to exit.
    Keyword arguments:
        maze -- Maze as returned from read_maze function.
        x    -- An "x" coordinate of the cell.
        y    -- An "y" coordinate of the cell.
    Returns:
        True if (x, y) is exit out the maze, False otherwise.'''
    if x == 0 or x + 1 == maze.shape[0]:
        return True
    if y == 0 or y + 1 == maze.shape[1]:
        return True
    return False
